//Nav_fixed
$(window).on('scroll',function() {
	var scrolltop = $(this).scrollTop();

	if(scrolltop >= 50) {
	  $('.navClient').addClass('fixed-client');
	}

	else if(scrolltop <= 50) {
	  $('.navClient').removeClass('fixed-client');
	}

	if(scrolltop >= 1200) {
    	$(".animated").addClass("fadeInUp");
    }	

});

$('.collapse a').on('click', function(){
    $('.navbar-toggler').click();
});

$('#modalInscricao').modal('show');

$('a.scroll_nav').on('click',function (e) {
    // e.preventDefault();

    var target = this.hash,
    $target = $(target);

   $('html, body').stop().animate({
     'scrollTop': $target.offset().top-80
    }, 2500, 'swing', function () {
     window.location.hash = target;
    });
});