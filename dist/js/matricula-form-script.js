$("#matriculaForm").validator().on("submit", function (event) {
    if (event.isDefaultPrevented()) {
        // handle the invalid form...
        formError();
        submitMSG(false, "Você precisa preencher o formulário corretamente");
    } else {
        // everything looks good!
        event.preventDefault();
        submitForm();
    }
});


function submitForm(){
    // Initiate Variables With Form Content
    var name_mat = $("#name_mat").val();
    var email_mat = $("#email_mat").val();
    var telefone_mat = $("#telefone_mat").val();
    var cpf_mat = $("#cpf_mat").val();
    var cep_mat = $("#cep_mat").val();
    var log_mat = $("#logradouro_mat").val();
    var num_mat = $("#num_mat").val();
    var uf_mat = $("#uf_mat").val();
    var cpmp_mat = $("#comp_mat").val();
    var cidade_mat = $("#cidade_mat").val();
    var bairro_mat = $("#bairro_mat").val();
    const COURSEID = "971248";
    const URL = 'https://hooks.zapier.com/hooks/catch/8755223/oqoayb2';

    var data = {
        "courseId" : COURSEID,
        "e-mail": email_mat,
        "name": name_mat,
        "cpf": cpf_mat,
        "cep": cep_mat,
        "logradouro": log_mat,
        "numero": num_mat,
        "uf": uf_mat,
        "complemento": cpmp_mat,
        "cidade": cidade_mat,
        "bairro": bairro_mat
    }


    $.ajax({
        type: "POST",
        url: URL,
        data: data,
        dataType: 'json',
        success : function(success){
            formSuccess();
        },
        error: function(e) {
            formError();
            submitMSG(false, "Error ao realizar o cadastro, tente novamente mais tarde!");
        }
    });
}

function formSuccess(){
    $("#matriculaForm")[0].reset();
    submitMSG(true, "Mensagem enviada com sucesso!")
}

function formError(){
    $("#matriculaForm").removeClass().addClass('shake animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
        $(this).removeClass();
    });
}

function submitMSG(valid, msg){
    if(valid){
        var msgClasses = "h3 text-center tada animated text-success";
    } else {
        var msgClasses = "h3 text-center text-danger";
    }
    $("#msgSubmit_mat").removeClass().addClass(msgClasses).text(msg);
}