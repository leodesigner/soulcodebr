<?php

$errorMSG = "";

// NAME
if (empty($_POST["name"])) {
    $errorMSG = "Insira um nome ";
} else {
    $name = $_POST["name"];
}

// E-MAIL
if (empty($_POST["email"])) {
    $errorMSG = "Insira seu E-mail ";
} else {
    $email = $_POST["email"];
}

// Nickname
if (empty($_POST["empresa"])) {
    $errorMSG .= "Insira o nome da empresa";
} else {
    $empresa = $_POST["empresa"];
}

// Tassunto
if (empty($_POST["telefone"])) {
    $errorMSG .= "Insira um telefone";
} else {
    $telefone = $_POST["telefone"];
}


// MESSAGE
if (empty($_POST["message"])) {
    $errorMSG .= "A caixa de mensagem é origatória";
} else {
    $message = $_POST["message"];
}


//$EmailTo = "contato@soulcodebrasil.org";
$EmailTo = "contato@soulcodeacademy.org";
$Subject = "Contato - Soul Code Academy";

// prepare email body text
$Body = "";
$Body .= "Nome: ";
$Body .= $name;
$Body .= "\n";
$Body .= "E-mail: ";
$Body .= $email;
$Body .= "\n";
$Body .= "Empresa: ";
$Body .= $empresa;
$Body .= "\n";
$Body .= "Telefone: ";
$Body .= $telefone;
$Body .= "\n";
$Body .= "Mensagem: ";
$Body .= $message;
$Body .= "\n";

// send email
$success = mail($EmailTo, $Subject, $Body, "From:".$name);

// redirect to success page
if ($success && $errorMSG == ""){
   echo "success";
}else{
    if($errorMSG == ""){
        echo "Something went wrong :(";
    } else {
        echo $errorMSG;
    }
}

?>