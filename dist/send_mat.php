<?php

$errorMSG = "";

// NAME
if (empty($_POST["name_mat"])) {
    $errorMSG = "Insira um nome ";
} else {
    $name_mat = $_POST["name_mat"];
}

// E-MAIL
if (empty($_POST["email_mat"])) {
    $errorMSG = "Insira seu E-mail ";
} else {
    $email_mat = $_POST["email_mat"];
}

// CPF
if (empty($_POST["cpf_mat"])) {
    $errorMSG .= "Insira seu CPF";
} else {
    $cpf_mat = $_POST["cpf_mat"];
}

// Telefone
if (empty($_POST["telefone_mat"])) {
    $errorMSG .= "Insira um telefone";
} else {
    $telefone_mat = $_POST["telefone_mat"];
}

// CEP
if (empty($_POST["cep_mat"])) {
    $errorMSG .= "Insira o CEP";
} else {
    $cep_mat = $_POST["cep_mat"];
}

// numero
if (empty($_POST["num_mat"])) {
    $errorMSG .= "Insira o Número";
} else {
    $num_mat = $_POST["num_mat"];
}

// uf
if (empty($_POST["uf_mat"])) {
    $errorMSG .= "Insira Seu estado";
} else {
    $uf_mat = $_POST["uf_mat"];
}

// complemento
if (empty($_POST["comp_mat"])) {
    $errorMSG .= "Insira um complemento";
} else {
    $comp_mat = $_POST["comp_mat"];
}

// cidade
if (empty($_POST["cidade_mat"])) {
    $errorMSG .= "Insira sua cidade";
} else {
    $cidade_mat = $_POST["cidade_mat"];
}

// numero
if (empty($_POST["bairro_mat"])) {
    $errorMSG .= "Insira seu bairro";
} else {
    $bairro_mat = $_POST["bairro_mat"];
}


//$EmailTo = "contato@soulcodebrasil.org";
$EmailTo = "contato@soulcodeacademy.org";
$Subject = "Matricula - Soul Code Academy";

// prepare email body text
$Body = "";
$Body .= "Nome: ";
$Body .= $name_mat;
$Body .= "\n";
$Body .= "E-mail: ";
$Body .= $email_mat;
$Body .= "\n";
$Body .= "Telefone: ";
$Body .= $telefone_mat;
$Body .= "\n";
$Body .= "CPF: ";
$Body .= $cpf_mat;
$Body .= "\n";
$Body .= "CEP: ";
$Body .= $cep_mat;
$Body .= "\n";
$Body .= "Nº: ";
$Body .= $num_mat;
$Body .= "\n";
$Body .= "Complemento: ";
$Body .= $comp_mat;
$Body .= "\n";
$Body .= "Cidade: ";
$Body .= $cidade_mat;
$Body .= "\n";
$Body .= "Bairro: ";
$Body .= $bairro_mat;
$Body .= "\n";

// send email
$success = mail($EmailTo, $Subject, $Body, "From:".$name_mat);

// redirect to success page
if ($success && $errorMSG == ""){
   echo "success";
}else{
    if($errorMSG == ""){
        echo "Something went wrong :(";
    } else {
        echo $errorMSG;
    }
}

?>